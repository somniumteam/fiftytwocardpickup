#!/bin/bash

cd "$(dirname "$(readlink -f "$0")")"

OUT_DIR="$PWD/target/deploy"

rm -r "$OUT_DIR"/*

"$HOME/.cargo/bin/wasm-bindgen" \
    --no-typescript --target web \
    --out-dir "$OUT_DIR" \
    --out-name "fiftytwocardpickup" \
    ./target/wasm32-unknown-unknown/release/fiftytwocardpickup.wasm

cp -r assets "$OUT_DIR/assets"
mv "$OUT_DIR/assets/index.html" "$OUT_DIR"

cd "$OUT_DIR"
7z a fiftytwocardpickup.zip ./*