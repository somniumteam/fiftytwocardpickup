use bevy::prelude::*;
use bevy::window::PrimaryWindow;
use bevy_inspector_egui::*;
use bevy_inspector_egui::bevy_egui::*;

#[derive(Default, Resource, Debug, Reflect, Copy, Clone, PartialEq, Eq)]
struct DebugGuiOpen(bool);

pub fn setup_debug_gui(app: &mut App) {
    app.add_plugins((
        EguiPlugin,
        DefaultInspectorConfigPlugin,
    ))
        .add_systems(Update, toggle_debug_gui)
        .add_systems(Last, update_debug_gui)
        .init_resource::<DebugGuiOpen>();
}

fn toggle_debug_gui(mut gui_open: ResMut<DebugGuiOpen>, input: Res<Input<KeyCode>>) {
    if input.just_pressed(KeyCode::Grave) {
        let gui_open = gui_open.as_mut();
        gui_open.0 = !gui_open.0;
    }
}

fn update_debug_gui(world: &mut World) {
    let gui_open = world.resource::<DebugGuiOpen>();
    if !gui_open.0 {return;}

    let Ok(egui_context) = world
        .query_filtered::<&mut EguiContext, With<PrimaryWindow>>()
        .get_single(world)
    else {return;};

    let mut egui_context = egui_context.clone();
    let context = egui_context.get_mut();

    egui::Window::new("DEBUG_GUI").show(&context, |ui| {
        egui::ScrollArea::vertical().show(ui, |ui| {
            bevy_inspector::ui_for_world(world, ui);
        });
    });
}