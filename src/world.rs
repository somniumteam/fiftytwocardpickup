// This may need to be relocated at a later date, but for now i'm putting it next to the main
// Just to get some basic boilerplate going

use bevy::prelude::*;

pub struct ViewPortPlugin;

impl Plugin for ViewPortPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(Startup, setup);
    }
}

fn setup(
    mut commands: Commands
) {
    commands.spawn(Camera2dBundle::default());
}
