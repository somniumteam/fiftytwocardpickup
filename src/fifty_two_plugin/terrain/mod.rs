use bevy::prelude::*;
use rand::seq::IteratorRandom;
use super::*;

pub const PLAYER_LOAD_TERRAIN_DISTANCE: u64 = 5;
pub const INITIAL_LOAD_TERRAIN_DISTANCE: u64 = 12;
pub const DEFAULT_HEX_SPACING: f32 = 24.0;
pub const TERRAIN_GENERATION_ANIMATION_MILLIS: u64 = 1200;
pub const TERRAIN_HOVER_OFFSET: f32 = DEFAULT_HEX_SPACING / 4.0;
pub const TERRAIN_HOVER_ANIMATION_MILLIS: u64 = 150;
pub const TERRAIN_CLEAN_ANIMATION_MILLIS: u64 = 3000;

#[derive(Resource, Reflect)]
pub struct HexSpacing(pub f32);

#[derive(Resource)]
pub struct GrassSprite(pub SpriteBundle);

#[derive(Component, Default, Clone, Copy, Debug, PartialEq, Reflect)]
pub struct WorldPosition {
    pub position: HexCoordinate,
}

#[derive(Component)]
pub struct IsTerrain;

#[derive(Event)]
pub struct ChunkGenerateEvent {
    pub position: HexCoordinate,
    pub radius: u64,
}

#[derive(Event)]
pub struct TileGeneratedEvent(pub HexCoordinate);

pub struct TerrainPlugin;
impl Plugin for TerrainPlugin {
    fn build(&self, app: &mut App) {
        app.add_event::<ChunkGenerateEvent>();
        app.add_event::<TileGeneratedEvent>();
        app.insert_resource(HexSpacing(DEFAULT_HEX_SPACING));
        app.register_type::<HexSpacing>();
        app.add_systems(Startup, terrain_setup_system);
        app.add_systems(PreUpdate, inital_chunk_load_system);
        app.add_systems(FixedUpdate, (
            generate_player_terrain_system,
            generate_terrain_system
                .run_if(resource_exists::<GrassSprite>()),
        ));
        app.add_systems(Update, (
            terrain_interpolated_motion_system,
            terrain_hover_system,
            game_over_clean_terrain_system,
        ));
        app.add_systems(PostUpdate, (
            terrain_hover_removed_system,
        ));
    }
}

fn terrain_setup_system(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    hex_spacing: Res<HexSpacing>,
) {
    let grass_scale = hex_spacing.0 / 55.0;
    let grass_asset: Handle<Image> = asset_server.load("Hexagon Grass.png");
    let grass_sprite = SpriteBundle {
        texture: grass_asset,
        transform: Transform {
            scale: Vec3::splat(grass_scale),
            ..Default::default()
        },
        ..Default::default()
    };
    commands.insert_resource(GrassSprite(grass_sprite));
}

fn inital_chunk_load_system(
    mut game_state_events: EventReader<GameStateChangeEvent>,
    mut terrain_generate: EventWriter<ChunkGenerateEvent>,
) {
    for event in game_state_events.read() {
        if event.0 == GameState::PLAYING {
            terrain_generate.send(ChunkGenerateEvent {
                position: HexCoordinate::default(),
                radius: INITIAL_LOAD_TERRAIN_DISTANCE,
            });
        }
    }
}

fn generate_player_terrain_system(
    mut player_moved: EventReader<PlayerMovedEvent>,
    mut terrain_generate: EventWriter<ChunkGenerateEvent>,
) {
    for player_moved in player_moved.read() {
        terrain_generate.send(ChunkGenerateEvent {
            position: player_moved.0,
            radius: PLAYER_LOAD_TERRAIN_DISTANCE,
        })
    }
}

fn generate_terrain_system(
    mut terrain_events: EventReader<ChunkGenerateEvent>,
    mut commands: Commands,
    query: Query<(
        &WorldPosition,
        With<IsTerrain>,
    )>,
    mut tile_events: EventWriter<TileGeneratedEvent>,
    grass_sprite: Res<GrassSprite>,
    hex_spacing: Res<HexSpacing>,
) {
    if let Some(generate_event) = terrain_events.read().next() {
        let destination = generate_event.position;
        let spiral = HexCoordinate::spiral(Some(generate_event.radius));
        let spiral_iter = std::iter::from_generator(spiral).map(|coordinate| {
            HexCoordinate {
                q: coordinate.q + destination.q,
                r: coordinate.r + destination.r,
            }
        });
        for destination in spiral_iter {
            if query.iter().find(|(pos, ())| pos.position == destination).is_none() {
                let spawn_position = destination.to_euclidean() * hex_spacing.0;
                let spawn_position = Vec3 {x: spawn_position.x, y: spawn_position.y - 500.0, z: 0.0};
                commands.spawn(grass_sprite.0.clone()).insert((
                    WorldPosition {position: destination},
                    Transform {
                        translation: spawn_position,
                        ..grass_sprite.0.transform
                    },
                    Name::new("Grass Tile"),
                    IsTerrain,
                ));
                tile_events.send(TileGeneratedEvent(destination));
            }
        }
    }
}

fn terrain_interpolated_motion_system(
    query: Query<(
        Entity,
        &Transform,
        &WorldPosition,
        Added<WorldPosition>,
        Changed<WorldPosition>,
        Without<InterpolatedMotion>,
        With<IsTerrain>,
    )>,
    mut commands: Commands,
    hex_spacing: Res<HexSpacing>,
) {
    for (
        entity,
        transform,
        position,
        added_position,
        changed_position,
        (),
        (),
    ) in query.iter() {
        if added_position || changed_position {
            let position = position.position.to_euclidean() * hex_spacing.0;
            commands.entity(entity).insert(
                InterpolatedMotion {
                    origin: transform.translation,
                    destination: Vec3 {x: position.x, y:position.y, z: 0.0},
                    duration: Duration::from_millis(TERRAIN_GENERATION_ANIMATION_MILLIS),
                    ..Default::default()
                }
            );
        }
    }
}

fn terrain_hover_system(
    mut commands: Commands,
    mut query: Query<(
        Entity,
        &Transform,
        &WorldPosition,
        Option<&mut InterpolatedMotion>,
        Added<IsCursorOverHex>,
        Option<&IsActor>,
        Without<DropoffDeletion>,
    )>,
    hex_spacing: Res<HexSpacing>,
) {
    for (entity, transform, world_position, interpolated_motion, has_cursor, is_actor, ()) in query.iter_mut() {
        if has_cursor {
            let destination = world_position.position.to_euclidean() * hex_spacing.0;
            let destination = Vec3 {
                x: destination.x,
                y: destination.y + TERRAIN_HOVER_OFFSET,
                z: transform.translation.z + 0.1 + if is_actor.is_some() {1.0} else {0.0},
                ..Default::default()
            };
            if let Some(mut interpolated_motion) = interpolated_motion {
                interpolated_motion.destination = destination;
            } else {
                commands.entity(entity).insert(
                    InterpolatedMotion {
                        origin: transform.translation,
                        destination,
                        duration: Duration::from_millis(TERRAIN_HOVER_ANIMATION_MILLIS),
                        ..Default::default()
                    },
                );
            }
        }
    }
}

fn terrain_hover_removed_system(
    mut commands: Commands,
    mut removed: RemovedComponents<IsCursorOverHex>,
    query: Query<(
        Entity,
        &Transform,
        &WorldPosition,
        Option<&IsActor>,
    )>,
    hex_spacing: Res<HexSpacing>,
) {
    let removed_ents: Vec<_> = removed.read().collect();
    for (entity, transform, world_position, is_actor) in query.iter() {
        if removed_ents.contains(&entity) {
            let new_position = world_position.position.to_euclidean() * hex_spacing.0;
            let new_position = Vec3 {
                x: new_position.x,
                y: new_position.y,
                z: if is_actor.is_some() {1.0} else {0.0}
            };
            commands.entity(entity).insert(
                InterpolatedMotion {
                    origin: transform.translation,
                    destination: new_position,
                    duration: Duration::from_millis(TERRAIN_HOVER_ANIMATION_MILLIS),
                    ..Default::default()
                }
            );
        }
    }
}

fn game_over_clean_terrain_system(
    mut commands: Commands,
    query: Query<(
        Entity,
        &Transform,
        With<IsTerrain>
    )>,
    game_state: ResMut<GameState>,
    enemies: Query<With<AttackerAI>>,
) {
    if *game_state.as_ref() != GameState::ENDED || !enemies.is_empty() {return;}
    let mut rng = rand::thread_rng();
    let chosen: Vec<_> = query.iter().choose_multiple(&mut rng, 3);
    for (entity, transform, ()) in chosen.iter() {
        commands.entity(*entity)
            .remove::<IsTerrain>()
            .insert((
                DropoffDeletion,
                InterpolatedMotion {
                    origin: transform.translation,
                    destination: transform.translation + Vec3::new(0.0, -1200.0, 0.0),
                    duration: Duration::from_millis(TERRAIN_CLEAN_ANIMATION_MILLIS),
                    ..Default::default()
                }
            ));
    }
}