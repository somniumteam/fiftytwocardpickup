use std::time::Duration;
use bevy::prelude::*;

pub struct InterpolatedMotionPlugin;

impl Plugin for InterpolatedMotionPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(Update, interpolate_motion_system);
    }
}

#[derive(Component, Reflect, Default)]
pub struct InterpolatedMotion {
    pub origin: Vec3,
    pub destination: Vec3,
    pub duration: Duration,
    pub elapsed: Duration,
}

fn interpolate_motion_system(
    mut commands: Commands,
    time: Res<Time>,
    mut query: Query<(
        Entity,
        &mut Transform,
        &mut InterpolatedMotion,
    )>,
) {
    for (
        entity,
        mut transform,
        mut interpolation,
    ) in &mut query {
        let transform = transform.as_mut();
        let InterpolatedMotion {
            ref origin,
            ref destination,
            ref duration,
            elapsed,
        } = interpolation.as_mut();
        *elapsed += time.delta();
        let ratio = elapsed.as_secs_f32() / duration.as_secs_f32();
        if ratio <= 1.0 {
            let ratio = easing_in_out_back(ratio);
            transform.translation = Vec3::lerp(*origin, *destination, ratio);
            continue;
        } else {
            transform.translation = *destination;
            commands.entity(entity).remove::<InterpolatedMotion>();
        }
    }
}

fn easing_in_out_back(x: f32) -> f32 {
    let c1: f32 = 1.70158;
    let c2: f32 = c1 * 1.525;

    if x < 0.5 {
        (f32::powf(2.0 * x, 2.0) * ((c2 + 1.0) * 2.0 * x - c2)) / 2.0
    } else {
        (f32::powf(2.0 * x - 2.0, 2.0) * ((c2 + 1.0) * (x * 2.0 - 2.0) + c2) + 2.0) / 2.0
    }
}