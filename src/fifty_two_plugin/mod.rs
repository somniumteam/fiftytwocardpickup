pub mod hexagons;
pub mod interpolation;
pub mod entities;
pub mod terrain;
pub mod ui;
pub mod sound;

pub use hexagons::HexCoordinate;
pub use interpolation::*;
pub use hexagons::input::*;
pub use entities::actors::*;
pub use terrain::*;
pub use ui::*;
pub use sound::*;
use std::time::Duration;
use bevy::prelude::*;

#[derive(Resource, Default, Reflect, PartialEq, Copy, Clone)]
pub enum GameState {
    PLAYING,
    #[default]
    ENDED,
}

#[derive(Event)]
pub struct GameStateChangeEvent(pub GameState);

pub struct FiftyTwoPlugin;
impl Plugin for FiftyTwoPlugin {
    fn build(&self, app: &mut App) {
        app.add_event::<GameStateChangeEvent>();
        app.init_resource::<GameState>();
        app.add_plugins((
            InterpolatedMotionPlugin,
            HexInputPlugin,
            ActorsPlugin,
            TerrainPlugin,
            UIPlugin,
            SoundPlugin,
        ));
        app.add_systems(First, (
            start_game_system.before(change_game_state_system),
            change_game_state_system,
        ));
        app.add_systems(Last, (
            dropoff_deletion_system,
        ));
        app.world.send_event(GameStateChangeEvent(GameState::PLAYING));
    }
}

#[derive(Component, Reflect)]
pub struct DropoffDeletion;

fn dropoff_deletion_system(
    mut commands: Commands,
    query: Query<(
        Entity,
        Without<InterpolatedMotion>,
        With<DropoffDeletion>,
    )>,
) {
    for (entity, (), ()) in query.iter() {
        commands.entity(entity).despawn_recursive();
    }
}

fn change_game_state_system(
    mut game_state: ResMut<GameState>,
    mut events: EventReader<GameStateChangeEvent>,
) {
    for event in events.read() {
        *game_state = event.0;
    }
}

fn start_game_system(
    game_state: Res<GameState>,
    buttons: Res<Input<MouseButton>>,
    mut events: EventWriter<GameStateChangeEvent>,
) {
    if *game_state == GameState::ENDED && buttons.just_released(MouseButton::Left) {
        events.send(GameStateChangeEvent(GameState::PLAYING));
    }
}