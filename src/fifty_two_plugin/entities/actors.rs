use super::super::*;
use bevy::audio::*;
use bevy::prelude::*;
use bevy::sprite::*;
use bevy::text::Text2dBounds;
use hexagons::input::*;
use rand::seq::IteratorRandom;

pub const MINIMUM_ENEMY_SPAWN_DISTANCE: f32 = 5.0;
pub const PLAYER_MOVEMENT_ENEMY_SPAWN_COUNT: u64 = 2;
pub const DEFAULT_MAX_HEALTH: u64 = 100;
pub const ENEMY_DEATH_ANIMATION_MILLIS: u64 = 1200;
pub const ENEMY_SIZE_HEXES: f32 = 0.4;

#[derive(Bundle, Default)]
#[bundle()]
pub struct AttackerEnemyBundle {
    pub name: Name,
    pub world_position: WorldPosition,
    pub sprite: MaterialMesh2dBundle<ColorMaterial>,
    pub is_actor: IsActor,
    pub attacker_ai: AttackerAI,
    pub strength: EnemyStrength,
}

#[derive(Component, Reflect)]
pub struct EnemyStrength(pub u64);
impl Default for EnemyStrength {
    fn default() -> Self {EnemyStrength(1)}
}

#[derive(Resource)]
pub struct EnemyStrengthFont(Handle<Font>);

#[derive(Component, Default)]
pub struct IsActor;

#[derive(Component, Reflect)]
pub struct Player {
    pub max_health: u64,
    pub health: u64,
}
impl Default for Player {
    fn default() -> Self {
        Self {
            max_health: DEFAULT_MAX_HEALTH,
            health: DEFAULT_MAX_HEALTH,
        }
    }
}

#[derive(Event)]
pub struct PlayerMovedEvent(pub HexCoordinate);

#[derive(Event)]
pub struct EnemySpawnEvent(pub HexCoordinate);

#[derive(Component, Default, Reflect)]
pub struct AttackerAI;

#[derive(Event)]
pub struct EnemyCollisionEvent {
    pub lhs: Entity,
    pub rhs: Entity,
}

#[derive(Resource)]
pub struct EnemySprite(MaterialMesh2dBundle<ColorMaterial>);

pub struct ActorsPlugin;
impl Plugin for ActorsPlugin {
    fn build(&self, app: &mut App) {
        app.add_event::<PlayerMovedEvent>();
        app.add_event::<EnemyCollisionEvent>();
        app.add_event::<EnemySpawnEvent>();
        app.register_type::<EnemyStrength>();
        app.register_type::<Player>();
        app.add_systems(Startup, setup_system);
        app.add_systems(PreUpdate, start_playing_system);
        app.add_systems(FixedUpdate, (
            move_player_system,
            attacker_ai_system,
            player_movement_enemy_spawn_system,
            enemy_spawn_system,
            detect_enemy_collisions_system,
            merge_enemies,
            game_over_system,
        ));
        app.add_systems(Update, (
            actor_interpolate_motion_system,
            update_enemy_strength_display
                .run_if(resource_exists::<EnemyStrengthFont>()),
            game_over_clean_enemies_system,
            player_movement_sound_system,
        ));
        app.add_systems(PostUpdate, remove_dead_enemies);
    }
}

fn setup_system(
    mut commands: Commands,
    assets: Res<AssetServer>,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
    hex_spacing: Res<HexSpacing>,
) {
    commands.spawn((
        MaterialMesh2dBundle {
            mesh: meshes.add(shape::Circle::new(hex_spacing.0 / 3.0).into()).into(),
            material: materials.add(ColorMaterial::from(Color::BLUE)),
            ..MaterialMesh2dBundle::default()
        },
        WorldPosition::default(),
        IsActor::default(),
        Player::default(),
    ));
    let enemy_font: Handle<Font> = assets.load("Oswald/Oswald-Bold.ttf");
    commands.insert_resource(EnemyStrengthFont(enemy_font));
    commands.insert_resource(EnemySprite(MaterialMesh2dBundle {
        mesh: meshes.add(shape::Circle::new(ENEMY_SIZE_HEXES * hex_spacing.0 / 2.0).into()).into(),
        material: materials.add(ColorMaterial::from(Color::RED)),
        ..Default::default()
    }));
}

fn start_playing_system(
    mut game_state_events: EventReader<GameStateChangeEvent>,
    mut player: Query<(
        &mut Player,
        &mut WorldPosition,
    )>,
) {
    for event in game_state_events.read() {
        if event.0 == GameState::PLAYING {
            let (mut player, mut world_position) = player.single_mut();
            player.max_health = DEFAULT_MAX_HEALTH;
            player.health = player.max_health;
            world_position.position = HexCoordinate::default();
        }
    }
}

fn actor_interpolate_motion_system(
    mut commands: Commands,
    mut query: Query<(
        Entity,
        &mut Transform,
        &WorldPosition,
        Added<WorldPosition>,
        Changed<WorldPosition>,
        With<IsActor>,
    )>,
    hex_spacing: Res<HexSpacing>,
) {
    for (entity, mut transform, position, added, changed, ()) in query.iter_mut() {
        if added || changed {
            let destination = position.position.to_euclidean() * hex_spacing.0;
            let destination = Vec3 {x: destination.x, y: destination.y, z: 1.0};
            if added {
                transform.translation = destination;
            } else {
                commands.entity(entity).insert(
                    InterpolatedMotion {
                        origin: transform.translation,
                        destination,
                        duration: Duration::from_millis(150),
                        ..Default::default()
                    },
                );
            }
        }
    }
}

fn move_player_system(
    mut query: Query<(
        &mut WorldPosition,
        With<Player>,
    )>,
    cursor: Res<CursorPosition>,
    buttons: Res<Input<MouseButton>>,
    game_state: Res<GameState>,
    mut player_moved_events: EventWriter<PlayerMovedEvent>,
) {
    if *game_state.as_ref() != GameState::PLAYING {return;}
    if buttons.just_released(MouseButton::Left) && cursor.hex.is_some() {
        let (mut world_position, ()) = query.single_mut();
        let player_hex = world_position.position.round();
        let cursor_hex = cursor.hex.unwrap();
        let destination = player_hex + (cursor_hex - player_hex).reduce_to_adjacent();
        if destination != player_hex {
            world_position.as_mut().position = destination;
            player_moved_events.send(PlayerMovedEvent(destination));
        }
    }
}

fn player_movement_enemy_spawn_system(
    mut player_events: EventReader<PlayerMovedEvent>,
    mut spawn_events: EventWriter<EnemySpawnEvent>,
    query: Query<(
        &WorldPosition,
        With<IsTerrain>,
        Without<InterpolatedMotion>,
    )>,
) {
    for player_moved in player_events.read() {
        let player_position = player_moved.0;
        let mut rng = rand::thread_rng();
        let mut spawns_remaining = PLAYER_MOVEMENT_ENEMY_SPAWN_COUNT;
        for _ in 0..1000 {
            if let Some(chosen) = query.iter().choose_stable(&mut rng) {
                let (chosen, _, _) = chosen;
                if chosen.position.manhattan_distance(player_position) >= MINIMUM_ENEMY_SPAWN_DISTANCE {
                    spawn_events.send(EnemySpawnEvent(chosen.position));
                    spawns_remaining -= 1;
                    if spawns_remaining == 0 {break;}
                }
            } else {break;}
        }
    }
}

fn player_movement_sound_system(
    mut commands: Commands,
    mut player_events: EventReader<PlayerMovedEvent>,
    sound: Res<PlayerMoveSound>,
) {
    if player_events.read().next().is_some() {
        commands.spawn(AudioBundle {
            source: sound.0.clone(),
            settings: PlaybackSettings {
                volume: Volume::Relative(VolumeLevel::new(0.2)),
                ..Default::default()
            },
            ..default()
        });
    }
}

fn enemy_spawn_system(
    mut commands: Commands,
    mut spawn_events: EventReader<EnemySpawnEvent>,
    hex_spacing: Res<HexSpacing>,
    enemy_sprite: Res<EnemySprite>,
) {
    for event in spawn_events.read() {
        commands.spawn(AttackerEnemyBundle {
            name: Name::new("Attacker Enemy"),
            sprite: enemy_sprite.0.clone(),
            world_position: WorldPosition {position: event.0},
            ..Default::default()
        }).with_children(|parent| {
            parent.spawn(Text2dBundle {
                text_anchor: Anchor::Center,
                text_2d_bounds: Text2dBounds {
                    size: Vec2::splat(hex_spacing.0),
                },
                transform: Transform {
                    translation: Vec3::new(0.0, 0.0, 1.0),
                    scale: Vec3::splat(ENEMY_SIZE_HEXES),
                    ..default()
                },
                ..default()
            });
        });
    }
}

fn attacker_ai_system(
    mut ai_query: Query<(
        &mut WorldPosition,
        &mut EnemyStrength,
        With<AttackerAI>,
    )>,
    mut player_moved_events: EventReader<PlayerMovedEvent>,
    mut player: Query<&mut Player>,
    damage_sound: Res<PlayerDamageSound>,
    mut sound_events: EventWriter<ExclusiveSoundEvent>,
) {
    if let Some(player_moved) = player_moved_events.read().next() {
        let player_position = player_moved.0;
        for (mut ai_position, mut strength, ()) in ai_query.iter_mut() {
            let ai_position = &mut ai_position.position;
            if ai_position.manhattan_distance(player_position) > 1.0 {
                let delta = player_position - *ai_position;
                let next_hop = delta.reduce_to_adjacent();
                *ai_position += next_hop;
            }
            else {
                let mut player = player.single_mut();
                for _ in 0..strength.0.min(player.health) {
                    sound_events.send(ExclusiveSoundEvent {
                        sound: damage_sound.0.clone(),
                        settings: PlaybackSettings {
                            volume: Volume::Relative(VolumeLevel::new(0.4)),
                            ..default()
                        },
                        ..default()
                    });
                }
                if player.health >= strength.0 {
                    player.health -= strength.0;
                } else {
                    player.health = 0;
                }
                strength.0 = 0;
            }
        }
    }
}

fn update_enemy_strength_display(
    query: Query<(
        &EnemyStrength,
        &Children,
    )>,
    mut text_query: Query<(
        &mut Text,
        &Parent,
    )>,
    enemy_font: Res<EnemyStrengthFont>,
) {
    for (strength, children) in query.iter() {
        for child in children.iter() {
            if text_query.contains(*child) {
                let (mut text, _) = text_query.get_mut(*child).expect("Failed to query for child text.");
                text.sections.clear();
                text.sections.push(TextSection {
                    value: strength.0.to_string(),
                    style: TextStyle {
                        font: enemy_font.0.clone(),
                        font_size: 36.0,
                        color: Color::BLACK,
                    },
                });
            }
        }
    }
}

fn detect_enemy_collisions_system(
    query: Query<(
        Entity,
        &WorldPosition,
        With<AttackerAI>,
    )>,
    mut collisions: EventWriter<EnemyCollisionEvent>,
) {
    for (lhs, lhs_position, ()) in query.iter() {
        let mut hold = true;
        for (rhs, rhs_position, ()) in query.iter() {
            if rhs == lhs {hold = false;}
            else if !hold && lhs_position.position == rhs_position.position {
                collisions.send(EnemyCollisionEvent {lhs, rhs});
            }
        }
    }
}

fn merge_enemies(
    mut commands: Commands,
    mut collisions: EventReader<EnemyCollisionEvent>,
    mut query: Query<(
        &mut EnemyStrength,
        &mut Transform,
    )>,
) {
    for collision in collisions.read() {
        let &EnemyCollisionEvent {lhs, rhs} = collision;
        if query.contains(lhs) && query.contains(rhs) {
            let rhs_strength = query.get(rhs).unwrap().0.0;
            let (mut lhs_strength, mut lhs_transform) = query.get_mut(lhs).unwrap();
            let lhs_strength = lhs_strength.as_mut();
            lhs_strength.0 += rhs_strength;
            lhs_transform.scale = Vec3::splat((lhs_strength.0 as f32).powf(0.2).min(2.0));
            commands.entity(rhs).despawn_recursive();
            break;
        }
    }
}

fn remove_dead_enemies(
    mut commands: Commands,
    query: Query<(
        Entity,
        &EnemyStrength,
        &Transform,
        With<IsActor>,
    )>,
) {
    for (entity, strength, transform, ()) in query.iter() {
        if strength.0 == 0 {
            commands.entity(entity)
                .remove::<IsActor>()
                .remove::<EnemyStrength>()
                .remove::<AttackerAI>()
                .insert((
                    DropoffDeletion,
                    InterpolatedMotion {
                        duration: Duration::from_millis(ENEMY_DEATH_ANIMATION_MILLIS),
                        origin: transform.translation,
                        destination: transform.translation + Vec3::new(0.0, -1200.0, 0.0),
                        ..Default::default()
                    },
                ));
        }
    }
}

fn game_over_system(
    player: Query<&Player>,
    game_state: ResMut<GameState>,
    mut game_state_events: EventWriter<GameStateChangeEvent>,
) {
    let player = player.single();
    if *game_state == GameState::PLAYING && player.health == 0 {
        game_state_events.send(GameStateChangeEvent(GameState::ENDED));
    }
}

fn game_over_clean_enemies_system(
    mut query: Query<(
        &mut EnemyStrength,
        With<IsActor>,
    )>,
    game_state: ResMut<GameState>,
) {
    if *game_state.as_ref() != GameState::ENDED {return;}
    if let Some((mut enemy, ())) = query.iter_mut().next() {
        enemy.0 = 0;
    }
}