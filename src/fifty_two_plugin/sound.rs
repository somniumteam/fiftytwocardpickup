use std::time::Duration;
use bevy::audio::*;
use bevy::prelude::*;
use bevy::audio::Volume;
use bevy::time::common_conditions::on_timer;
use super::*;

pub const MAX_EXCLUSIVE_SOUND_QUEUE: usize = 10;

pub struct SoundPlugin;

impl Plugin for SoundPlugin {
    fn build(&self, app: &mut App) {
        app.add_event::<ExclusiveSoundEvent>();
        app.init_resource::<ExclusiveSoundQueue>();
        app.add_systems(Startup, sound_setup_system);
        app.add_systems(Update, game_state_sound_system);
        app.add_systems(PostUpdate, (
            exclusive_sound_event_responder_system,
            exclusive_sound_player_system
                .run_if(on_timer(Duration::from_millis(200))),
        ));
    }
}

#[derive(Resource)]
pub struct GameEndSound(pub Handle<AudioSource>);

#[derive(Resource)]
pub struct PlayerMoveSound(pub Handle<AudioSource>);

#[derive(Resource)]
pub struct PlayerDamageSound(pub Handle<AudioSource>);

#[derive(Resource)]
pub struct GameMusicSound(pub Handle<AudioSource>);

#[derive(Component)]
pub struct IsGameMusic;

#[derive(Event, Default, Clone)]
pub struct ExclusiveSoundEvent {
    pub sound: Handle<AudioSource>,
    pub settings: PlaybackSettings,
}

#[derive(Resource, Default)]
pub struct ExclusiveSoundQueue(pub Vec<ExclusiveSoundEvent>);

fn sound_setup_system(
    assets: Res<AssetServer>,
    mut commands: Commands,
) {
    commands.insert_resource(GameEndSound(
        assets.load::<AudioSource>("soundimage/PowerRez4.ogg")
    ));
    commands.insert_resource(PlayerMoveSound(
        assets.load::<AudioSource>("soundimage/UI_Quirky22.ogg")
    ));
    commands.insert_resource(PlayerDamageSound(
        assets.load::<AudioSource>("soundimage/UI_Quirky33.ogg")
    ));
    let game_music = assets.load::<AudioSource>("soundimage/Frantic-Gameplay_v001.ogg");
    commands.insert_resource(GameMusicSound(game_music.clone()));
    commands.spawn((
        AudioBundle {
            source: game_music,
            settings: PlaybackSettings {
                mode: PlaybackMode::Loop,
                paused: true,
                volume: Volume::Relative(VolumeLevel::new(0.1)),
                ..Default::default()
            },
            ..Default::default()
        },
        IsGameMusic,
    ));
}

fn game_state_sound_system(
    mut events: EventReader<GameStateChangeEvent>,
    start_sound: Res<GameEndSound>,
    mut commands: Commands,
    music_controller: Query<&AudioSink, With<IsGameMusic>>,
) {
    if let Ok(sink) = music_controller.get_single() {
        for event in events.read() {
            if event.0 == GameState::ENDED {
                commands.spawn(AudioBundle {
                    source: start_sound.0.clone(),
                    ..default()
                });
            }
            if event.0 == GameState::PLAYING {sink.play();} else {sink.pause();}
        }
    }
}

fn exclusive_sound_event_responder_system(
    mut sound_events: EventReader<ExclusiveSoundEvent>,
    mut sound_queue: ResMut<ExclusiveSoundQueue>,
) {
    let queue = &mut sound_queue.as_mut().0;
    for event in sound_events.read() {
        if queue.len() <= MAX_EXCLUSIVE_SOUND_QUEUE {queue.push(event.clone());}
    }
}

fn exclusive_sound_player_system(
    mut commands: Commands,
    mut sound_queue: ResMut<ExclusiveSoundQueue>,
) {
    let queue = &mut sound_queue.as_mut().0;
    if !queue.is_empty() {
        let event = queue.remove(0);
        let sound = event.sound;
        let settings = event.settings;
        commands.spawn(AudioBundle {
            source: sound,
            settings,
            ..default()
        });
    }
}