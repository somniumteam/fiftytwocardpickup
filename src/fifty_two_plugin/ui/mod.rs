use bevy::prelude::*;
use bevy::sprite::*;
use bevy::text::*;
use bevy::window::*;
use super::*;
use super::entities::actors::*;

pub struct UIPlugin;
impl Plugin for UIPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(Startup, ui_setup_system);
        app.add_systems(Update, (
            update_health_bar_system,
            update_score_ui_system,
            update_game_end_ui_visibility_system,
        ));
    }
}

#[derive(Component)]
pub struct IsHealthBar;

#[derive(Component)]
pub struct IsCornerDocked;

#[derive(Component)]
pub struct IsScoreText;

#[derive(Component)]
pub struct IsGameEndUI;

pub const UI_HEALTH_BAR_WIDTH: f32 = 250.0;
pub const UI_HEALTH_BAR_HEIGHT: f32 = 50.0;
pub const UI_HEALTH_BAR_MARGIN: f32 = 20.0;
pub const UI_HEALTH_BAR_PADDING: f32 = 10.0;
pub const UI_GAME_END_MENU_WIDTH: f32 = 700.0;
pub const UI_GAME_END_MENU_HEIGHT: f32 = 250.0;

fn ui_setup_system(
    assets: Res<AssetServer>,
    mut commands: Commands,
) {
    commands.spawn((
        SpriteBundle {
            sprite: Sprite {
                color: Color::rgb(0.1, 0.0, 0.0),
                custom_size: Some(Vec2::new(UI_HEALTH_BAR_WIDTH, UI_HEALTH_BAR_HEIGHT)),
                ..default()
            },
            ..default()
        },
        IsCornerDocked,
    )).with_children(|parent| {
        parent.spawn((
            SpriteBundle {
                sprite: Sprite {
                    color: Color::rgb(1.0, 0.0, 0.0),
                    custom_size: Some(Vec2::new(
                        UI_HEALTH_BAR_WIDTH - UI_HEALTH_BAR_PADDING * 2.0,
                        UI_HEALTH_BAR_HEIGHT - UI_HEALTH_BAR_PADDING * 2.0
                    )),
                    ..default()
                },
                ..default()
            },
            IsHealthBar
        ));
    });
    let menu_size = Vec2::new(UI_GAME_END_MENU_WIDTH, UI_GAME_END_MENU_HEIGHT);
    let menu_font: Handle<Font> = assets.load("Oswald/Oswald-Bold.ttf");
    commands.spawn((
        SpriteBundle {
            sprite: Sprite {
                color: Color::rgb(0.5, 0.5, 0.5),
                custom_size: Some(menu_size),
                ..default()
            },
            ..default()
        },
        IsGameEndUI,
    )).with_children(|parent| {
        parent.spawn(Text2dBundle {
            text_anchor: Anchor::Center,
            text_2d_bounds: Text2dBounds {size: menu_size},
            transform: Transform::from_translation(Vec3::new(0.0, 80.0, 1.0)),
            text: Text::from_section(
                "GAME OVER",
                TextStyle {
                    font: menu_font.clone(),
                    font_size: 72.0,
                    color: Color::BLACK
                }
            ),
            ..Default::default()
        });
        parent.spawn((
            Text2dBundle {
                text_anchor: Anchor::Center,
                text_2d_bounds: Text2dBounds {size: menu_size},
                transform: Transform::from_translation(Vec3::new(0.0, -30.0, 1.0)),
                text: Text::from_section(
                    "...",
                    TextStyle {
                        font: menu_font.clone(),
                        font_size: 72.0,
                        color: Color::BLACK
                    }
                ),
                ..Default::default()
            },
            IsScoreText,
            EnemyStrength(0),
        ));
        parent.spawn((
            Text2dBundle {
                text_anchor: Anchor::Center,
                text_2d_bounds: Text2dBounds {size: menu_size},
                transform: Transform::from_translation(Vec3::new(0.0, -80.0, 1.0)),
                text: Text::from_section(
                    "Music and Sound by Eric Matyas: www.soundimage.org",
                    TextStyle {
                        font: menu_font.clone(),
                        font_size: 24.0,
                        color: Color::BLACK
                    }
                ),
                ..Default::default()
            },
        ));
    });
}

fn update_health_bar_system(
    player: Query<&Player>,
    mut health_bar: Query<(
        &mut Transform,
        With<IsHealthBar>,
        Without<IsCornerDocked>,
    )>,
    mut container: Query<(
        &mut Transform,
        With<IsCornerDocked>,
        Without<IsHealthBar>,
    )>,
    window: Query<&Window, With<PrimaryWindow>>,
) {
    let window = window.single();
    let width = window.width();
    let height = window.height();
    let player = player.single();
    let (mut transform, _, _) = health_bar.single_mut();
    transform.scale.x = (player.health as f32) / (player.max_health as f32);

    for (mut transform, _, _) in container.iter_mut() {
        transform.translation = Vec3::new(
            (-width + UI_HEALTH_BAR_WIDTH) / 2.0 + UI_HEALTH_BAR_MARGIN,
            (height - UI_HEALTH_BAR_HEIGHT) / 2.0 - UI_HEALTH_BAR_MARGIN,
            100.0
        );
    }
}

fn update_score_ui_system(
    mut text: Query<(
        &mut Text,
        With<IsScoreText>,
    )>,
    enemy_strength: Query<&EnemyStrength, Without<IsScoreText>>,
    mut prev_score: Query<&mut EnemyStrength, With<IsScoreText>>,
    game_state: Res<GameState>,
) {
    if *game_state == GameState::PLAYING {
        let score = enemy_strength.iter().map(|s| s.0).max().unwrap_or(0);
        let mut prev_score = prev_score.single_mut();
        let score = prev_score.0.max(score);
        prev_score.0 = score;
        let text = &mut text.single_mut().0.sections;
        if let Some(text) = text.get_mut(0) {
            text.value = format!("STRONGEST ENEMY: {}", score);
        }
    }
}

#[allow(unreachable_patterns)]
fn update_game_end_ui_visibility_system(
    mut menu: Query<(
        &mut Visibility,
        With<IsGameEndUI>,
    )>,
    mut game_state: EventReader<GameStateChangeEvent>,
    mut score: Query<(&mut EnemyStrength, With<IsScoreText>)>,
) {
    for events in game_state.read() {
        let (mut menu_visibility, ()) = menu.single_mut();
        let (mut prev_score, ()) = score.single_mut();
        match events.0 {
            GameState::ENDED => {
                *menu_visibility = Visibility::Inherited;
            },
            GameState::PLAYING => {
                *menu_visibility = Visibility::Hidden;
                prev_score.0 = 0;
            },
            _ => {}
        };
    }
}