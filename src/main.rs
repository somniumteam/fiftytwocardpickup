#![feature(generators)]
#![feature(generator_trait)]
#![feature(iter_from_generator)]

mod world;
mod fifty_two_plugin;
mod debug_gui;

use bevy::prelude::*;
use bevy::asset::AssetMetaCheck;
use fifty_two_plugin::*;
use world::ViewPortPlugin;

fn main() {
    let mut app = App::new();
    app.insert_resource(AssetMetaCheck::Never);
    app.add_plugins((
        DefaultPlugins.build()
            .set(WindowPlugin {
                primary_window: Some(Window {
                    canvas: Some("#canvas".into()),
                    fit_canvas_to_parent: true,
                    ..default()
                }),
                ..default()
            }
        ),
        ViewPortPlugin,
        fifty_two_plugin::FiftyTwoPlugin,
    ));
    app
        .register_type::<WorldPosition>()
        .register_type::<InterpolatedMotion>();
    debug_gui::setup_debug_gui(&mut app);
    app.run();
}
